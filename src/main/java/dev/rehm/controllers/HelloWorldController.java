package dev.rehm.controllers;

import dev.rehm.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Controller // this annotation registers the class as a bean, singleton scope by default
//@RestController // this annotation registers the class as a bean, singleton scope by default
@RequestMapping("/my-app")
public class HelloWorldController { //helloWorldController

    @Autowired
    Item i;

    @GetMapping
    @ResponseBody
    public String sayHello(){
        System.out.println(i);
        return "Hello World";
    }

}
