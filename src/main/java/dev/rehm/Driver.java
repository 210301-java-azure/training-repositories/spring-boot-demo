package dev.rehm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication // @Configuration + ...
public class Driver {

    public static void main(String[] args) {
        SpringApplication.run(Driver.class,args);

    }
}
